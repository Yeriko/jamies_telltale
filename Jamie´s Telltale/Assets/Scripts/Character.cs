﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]

public class Character {//: MonoBehaviour {


	public string characterName;
	/// <summary>
	/// The root is the container for all images related to the character in the scene. The root object
	/// </summary>
	[HideInInspector]public RectTransform root;

	public bool isMultilayerCharacter{get{ return renderers.renderer == null;}}
	public bool enabled {get{ return root.gameObject.activeInHierarchy;} set{ root.gameObject.SetActive (value);}}
	DialogueSystem dialogue;

	public void Say(string speech, bool add = false){
		if (!enabled)
			enabled = true;

		if (!add)
			dialogue.Say (speech, characterName);
		else
			dialogue.SayAdd (speech, characterName);
	}


	/// <summary>
	/// Create a new character
	/// </summary>
	/// <param name="_name">Name.</param>
	public Character (string _name, bool enableOnStart = true){
		CharacterManager cm = CharacterManager.instance;
		GameObject prefab = Resources.Load ("Characters/Character["+ _name+"]") as GameObject;
		//GameObject prefab = Resources.Load ("Characters/Character[" + _name + "]") as GameObject;
		//spawn an instance of the prefab directly on the character panel
		GameObject ob = GameObject.Instantiate (prefab, cm.characterPanel);

		root = ob.GetComponent<RectTransform> ();
		characterName = _name;

		//get the render
		renderers.renderer = ob.GetComponentInChildren<RawImage> ();
		if (isMultilayerCharacter) {
			renderers.bodyRenderer = ob.transform.Find ("bodyLayer").GetComponent<Image> ();
			renderers.expressionRenderer = ob.transform.Find ("expressionLayer").GetComponent<Image> ();
		}
		dialogue = DialogueSystem.instance;

		enabled = enableOnStart;
	}

	[System.Serializable]
	public class Renderers {
		/// <summary>
		/// Used as  the only image for a single layer character
		/// </summary>
		public RawImage renderer;

		///sprites uses image
		/// <summary>
		/// The body renderer for a multilayer
		/// </summary>
		public Image bodyRenderer;
		/// <summary>
		/// The expresion renderer for a multilayer character.
		/// </summary>
		public Image expressionRenderer;

	}
	public Renderers renderers = new Renderers();
}

