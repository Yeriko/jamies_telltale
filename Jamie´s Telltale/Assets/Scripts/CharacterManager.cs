﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// Responsible for maintaining a character in scene
/// </summary>
public class CharacterManager : MonoBehaviour {
	public static CharacterManager instance; 


/// <summary>
/// A list of all characters in scene
/// </summary>
	public RectTransform characterPanel;

	public List<Character> characters = new List<Character>();

		///<summary>
		///Easy lookup for our characters
		///</summary>
	public Dictionary<string, int> characterDictionary = new Dictionary<string, int>();

	void Awake(){
		instance = this;
	}
	///<summary>
	///Try to get the character by the name provided from the list
	///</summary>
	/// <param name="characterName">Character name</param>
	public Character GetCharacter (string characterName, bool createCharacterIfDoesNotExist = true, bool enableCreatedCharacterOnStart = true){
		//Search in our dictionary to find the  character quickly if it is already on the scene
		int index = -1;
		if (characterDictionary.TryGetValue (characterName, out index)){
			return characters [index];
	} else 
		{
			return CreateCharacter (characterName, enableCreatedCharacterOnStart);
		}
	}
	///<summary>
	///Try to get the character by the name provided from the list
	///</summary>
	/// <param name="characterName">Character name</param>
	public Character CreateCharacter (string characterName, bool enableOnStart = true)
	{
		Character newCharacter = new Character (characterName, enableOnStart);  

		characterDictionary.Add (characterName, characters.Count);
		characters.Add (newCharacter);

		return newCharacter; 
	}
}

