﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DialogueSystem : MonoBehaviour {
	public static DialogueSystem instance;
	public ELEMENTS elements;

	void Awake (){
		instance = this;
	}


	// Use this for initialization
	void Start () {
		
	}

	/// <summary>
	/// Say something and show it on the speech box
	/// </summary>
	public void Say(string speech, string speaker = ""){
		StopsSpeaking ();

		speaking = StartCoroutine (Speaking (speech, false, speaker)); 
	}


	/// <summary>
	/// Say something and show it on the speech box
	/// </summary>
	public void SayAdd(string speech, string speaker = ""){
		StopsSpeaking ();
		speechText.text = targetSpeech;
		speaking = StartCoroutine (Speaking (speech, true, speaker)); 
	}




	public void StopsSpeaking(){
		if (isSpeaking) {
			StopCoroutine (speaking);

		}
		speaking = null;
	}

	public bool isSpeaking{get{ return speaking != null;}}
	[HideInInspector]public bool isWaitingForUserInput= false;

		string targetSpeech = "";
		Coroutine speaking =  null;
	IEnumerator Speaking(string speech, bool additive, string speaker = ""){

		speechPanel.SetActive (true);
		targetSpeech = speech; 
		if (!additive) {
			speechText.text = "";
		} else {
			targetSpeech = speechText.text + targetSpeech;
		}

			speakerNameText.text = DetermineSpeaker(speaker); //temporary

		while (speechText.text != targetSpeech) {
			speechText.text += targetSpeech [speechText.text.Length];
				yield return new WaitForEndOfFrame();
		}
		//Text finished
		isWaitingForUserInput = true;
		while (isWaitingForUserInput)
			yield return new WaitForEndOfFrame();

		StopsSpeaking();

	}


	string DetermineSpeaker (string s){
		string retVal = speakerNameText.text; //Default return the last name
		if (s != speakerNameText.text && s != "")
			retVal = (s.ToLower ().Contains ("Narrador")) ? "" : s;
		return retVal;
	}

	/// <summary>
	/// Close the entire speech panel and stop dialogue.
	/// </summary>
	public void Close(){
		StopsSpeaking ();
		speechPanel.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
		
	}

	[System.Serializable]
	public class ELEMENTS{
		/// <summary>
		/// The main panel containing all dialogue related elements on the UI
		/// </summary>

		public GameObject speechPanel;
		public Text speakerNameText;
		public Text speechText;
	}

	public GameObject speechPanel {get{return elements.speechPanel;}}
	public Text speakerNameText {get{return elements.speakerNameText;}}
	public Text speechText {get{return elements.speechText;}}
}
