﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingScript : MonoBehaviour {
	DialogueSystem dialogue;
	// Use this for initialization
	void Start () {
		dialogue = DialogueSystem.instance;
	}

	public string [] s = new string[]{

		"Una señora desagradable, fea y antipática entra en una tienda con sus dos niños. Les grita, insulta, regaña, estruja y tironea sin parar :Narrador",
		"El gerente de la tienda se dirige amablemente a ella y le dice",
		"¡Buenos días señora, bienvenida a nuestra tienda!. Tiene usted dos hermosos niños, ¿son gemelos?. :Gerente",
		"La malvada señora deja un momento de gritar y con una mirada entre agria y burlona responde al encargado :Narrador",
		"¡Por supuesto que no !. El mayor tiene 9 años y el otro 7. ¿De dónde chingados saca usted que podrían ser gemelos?. ¿Es usted ciego o está pendejo?. :Señora",
		"A lo que el gerente responde :Narrador",
		"No señora, no soy ciego, ni pendejo... ¡simplemente no puedo creer queclase de ser humano es tan idiota para cogersela dos veces!. :Gerente"
	};

	int index = 0;
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			if (!dialogue.isSpeaking || dialogue.isWaitingForUserInput) {
				if (index >= s.Length) {
					return;
				}
				Say(s[index]);
				index++;
			}
		}


		//Se detecta el toque
		if (Input.touchCount == 1) 
		{
			if (Input.GetTouch (0).phase == TouchPhase.Began && (!dialogue.isSpeaking || dialogue.isWaitingForUserInput)) 
			{
				if (index >= s.Length) {
					return;
				}
				Say(s[index]);
				index++;
			}
		}
	}


	void Say (string s){
		string[] parts = s.Split (':');
		string speech = parts [0];
		string speaker = (parts.Length >= 2) ? parts [1] : "";

		dialogue.Say(speech, speaker);
			
	}
}
