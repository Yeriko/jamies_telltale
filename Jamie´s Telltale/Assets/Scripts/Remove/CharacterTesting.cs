﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterTesting : MonoBehaviour {
	public Character Shikamaru; 
	// Use this for initialization
	void Start () {
		Shikamaru = CharacterManager.instance.GetCharacter ("Shikamaru", enableCreatedCharacterOnStart:false);
	}

	public string[] speech;
	int i = 0;
	// Update is called once per frame
	void Update () {


		if (Input.GetKeyDown (KeyCode.Space)) {
			if (i < speech.Length)
				Shikamaru.Say(speech[i]);
			else
				DialogueSystem.instance.Close();

			i++;
		}


		//Se detecta el toque
		if (Input.touchCount == 1) {
			if (Input.GetTouch (0).phase == TouchPhase.Began) {
				if (i < speech.Length)
					Shikamaru.Say (speech [i]);
				else
					DialogueSystem.instance.Close ();

				i++;
			}
		}
		}
	}

